import { Observable } from 'rxjs';

export interface ILoginService {
    signIn(email, pass): Observable<object>;
}