import { TestBed } from '@angular/core/testing';
import { LoginService } from './login.service';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';

describe('Login Service', () => {
  let service: LoginService;
  let serviceMock: any;
  beforeEach(() => {
    serviceMock = jasmine.createSpyObj('LoginService', ['signIn'])
    serviceMock.signIn.and.returnValue(of({}));
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        { provide: LoginService }
      ]
    });
    service = TestBed.inject(LoginService);
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
