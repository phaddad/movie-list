import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ILoginService } from './login.interface'
import { LOGIN } from './mock/login'

@Injectable({ providedIn: 'root' })
export class LoginService implements ILoginService {
  constructor(private http: HttpClient) {
  }
  signIn(email, pass): Observable<object> {
    if (email === 'test@test.com' && pass === '123123') {
      return of(LOGIN);
    }
    return of({});
  }
}