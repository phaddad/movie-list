import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login/login.mock.service'

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    isAuthenticated = false;

    constructor(private router: Router, private login: LoginService) {
    }
    checkAuthenticated() {
        return this.isAuthenticated;
    }
    setAuthenticated(value) {
        this.isAuthenticated = value;
    }
    signIn(email: string, pass: string) {
        return this.login.signIn(email, pass);
    }


}