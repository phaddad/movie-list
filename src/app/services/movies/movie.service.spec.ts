import { TestBed } from '@angular/core/testing';
import { MovieService } from './movie.service';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';

describe('Movies Service', () => {
  let service: MovieService;
  let serviceMock: any;
  beforeEach(() => {
    serviceMock = jasmine.createSpyObj('MovieService', ['search'])
    serviceMock.search.and.returnValue(of([]));
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        { provide: MovieService }
      ]
    });
    service = TestBed.inject(MovieService);
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
