import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { IMovieService } from './movie.interface'
import { MOVIES } from './mock/movies'

@Injectable({ providedIn: 'root' })
export class MovieService implements IMovieService {
  constructor(private http: HttpClient) {
  }
  search(filter): Observable<object> {
    return of(MOVIES);
  }
}

