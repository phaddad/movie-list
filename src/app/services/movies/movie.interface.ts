import { Observable } from 'rxjs';

export interface IMovieService {
    search(filter): Observable<object>;
}