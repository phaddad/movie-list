import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IMovieService } from './movie.interface'
@Injectable({ providedIn: 'root' })
export class MovieService implements IMovieService {
  constructor(private http: HttpClient) {
  }

  search(filter): Observable<object> {
    return this.http.get<object>('/api/movies').pipe(
      catchError(this.handleError('search', []))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(error as T);
    };
  }
}

