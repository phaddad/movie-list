import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../services/movies/movie.mock.service'

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  movieList = [];
  constructor(private movieService: MovieService) {
  }
  ngOnInit() {
    this.movieService.search('').subscribe(movies => {
      this.movieList = movies["entries"].map(movie => {
        return {
          ...movie,
          imageUrl: movie.images["Poster Art"]["url"]
        }
      }).slice(1, 20);
    })
  }
}
