import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
    this.form = this.fb.group({
      username: ['', Validators.email],
      password: ['', Validators.required]
    });
  }
  ngOnInit() {
  }
  onSubmit() {
    const username = this.form.get('username')?.value;
    const password = this.form.get('password')?.value;
    this.authService.signIn(username, password)
      .subscribe(res => {
        if (res["status"] === "SUCCESS") {
          this.authService.setAuthenticated(true);
          this.router.navigate(['/']);
        }
      });
  }
}
