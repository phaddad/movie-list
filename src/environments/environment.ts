export const environment = {
  production: false,
  name: '(DEV)',
  baseUrl: 'http://localhost:4200',
  application: {
    login: '/api/login',
    movies: '/api/movies'
  },
  providers: []
};
