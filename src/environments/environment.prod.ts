import { MovieService } from '../app/services/movies/movie.service'
import { MovieService as MovieMockService } from '../app/services/movies/movie.mock.service'
import { LoginService } from '../app/services/login/login.service'
import { LoginService as LoginMockService } from '../app/services/login/login.mock.service'

export const environment = {
  production: true,
  name: '(prod)',
  baseUrl: 'http://production.com',
  application: {
    login: '/api/login',
    movies: 'api/movies'
  },
  providers: [
    { provide: MovieMockService, useClass: MovieService },
    { provide: LoginMockService, useClass: LoginService }
  ]
};
